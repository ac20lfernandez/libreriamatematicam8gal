<?php
/**
 * Clase que permite restar dos vectores
 */

class Restar {
     /**
      * Esta función devuelve la resta de dos vectores
      *@param Vector $a
      * @param Vector $b
      * @return Vector
      */
     function restar(Vector $a, Vector $b)
     {
         $i = $a->i - $a->i;
         $j = $b->j - $a->j;
         
         $vector= new Vector($i, $j);
         return $vector;
     }
 }
 ?>