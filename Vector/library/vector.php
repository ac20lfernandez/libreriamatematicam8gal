<?php

/**Clase que permite inicializar un vector con dos parámetros que representan sus coordenadas.
 */

class Vector {
    public $i;
    public $j;

    /**
     * Constructor de un vector
     * *@param integer $i
      * @param integer $j
      */
    
    function __construct($i, $j) {
        $this->i = $i;
        $this->j = $j;
      } 
  }
 ?>