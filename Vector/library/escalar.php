<?php
/**
 * Clase que permite multiplicar un vector por un escalar
 */
class Escalar {
     /**
      * Esta función devuelve el producto de un vector por un escalar
      * @param Vector $a
      * @param integer $escalar
      * @return Vector
      */
    function productePerEscalar(Vector $a, $escalar){
        return new Vector($a->i*$escalar, $a->j* $escalar);
    }
    
}
?>