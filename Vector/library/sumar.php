
<?php
/**
 * Clase que permite sumar dos vectores
 */

class Sumar {
    /**
     * Esta función devuelve la suma de dos vecores
     *@param Vector $a
     * @param Vector $b
     * @return Vector
     */
    function sumar(Vector $a, Vector $b)
    {
        $i = $a->i +$a->i;
        $j = $b->j +$a->j;
        
        $vector= new Vector($i, $j);
        return $vector;
    }
}
?>