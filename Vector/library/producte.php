<?php
/**Clase que contiene la función que permite calcular el producto de dos vectores
 */

class Producte{
    /**
     * La función retorna el producto de dos vectores
     * @param Vector $a
     * @param Vector $b
     * @return integer $producte
     */
    function producte(Vector $a, Vector $b)
    {
        {
            $producte = $a->i* $b->i + $a->j * $b->j;
            return $producte;
        } 
    }
}
?>