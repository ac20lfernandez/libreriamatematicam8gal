<?php
/**
 * Clase que permite calcular el vector unitario de un vector
 */

class Unitari {
    /**
     * Esta función devuelve el vector unitario de un vector
     * @param Vector $a
     * @return Vector
     */
    function unitari(Vector $a)
    {
        $modulo = sqrt($a->i*$a->i+$a->j+$a->j);
        return new Vector($a->i/$modulo, $a->j/$modulo);
        
    }
}
?>