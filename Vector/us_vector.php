<?php

require_once('library/escalar.php');
require_once('library/producte.php');
require_once('library/restar.php');
require_once('library/sumar.php');
require_once('library/unitari.php');
require_once('library/vector.php');


$vector1 = new Vector(1,2);
$vector2 = new Vector(1,4);

$sumar = new Sumar();
$restar = new Restar();
$productePerEscalar = new Escalar();
$producte = new Producte();
$unitari = new Unitari();

echo "Suma de los vectores: ";
print_r($sumar->sumar($vector1,$vector2));
echo "</br> ";
echo "</br> ";
echo "Resta de las fracciones: ";
print_r($restar->restar($vector1,$vector2));
echo "</br> ";
echo "</br> ";
echo "Producte per escalar: ";
print_r($productePerEscalar->productePerEscalar($vector1,3));
echo "</br> ";
echo "</br> ";
echo "Producte: ";
print_r($producte->producte($vector1,$vector2));
echo "</br> ";
echo "</br> ";
echo "Unitari: ";
print_r($unitari->unitari($vector1));
echo "</br> ";

?>