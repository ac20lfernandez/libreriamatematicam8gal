# LibreriaMatematicaM8GAL

Licencia de las librerías: CC BY

# Instalación con composer

Crea en la raíz de tu proyecto un archivo llamado composer.json que contenga esto:

```php
{
    "require": {
        "mathtrick/mathtrick": "dev-master"
    }
}
```

Descargar https://getcomposer.org/composer.phar
Ponlo en la raíz de tu proyecto y ejecuta el siguiente comando:

```php
php composer.phar install
```

# FRACCIONES

Esta librería sirve para hacer cálculos básicos entre dos fracciones: multiplicación, división, suma, resta y obtener la fracción irreductible

Ejemplo:

```php
<?php

require_once('library/fraccio.php');
require_once('library/suma.php');
require_once('library/restar.php');
require_once('library/multiplicar.php');
require_once('library/dividir.php');
require_once('library/obtenerIrreductible.php');


$fraccio1 = new Fraccion(1,2);
$fraccio2 = new Fraccion(1,4);

$sumar = new Sumar();
$restar = new Restar;
$multiplicar = new Multiplicar();
$dividir = new Dividir();
$irreductible = new Irreductible();

// "Suma de las fracciones: ";
$sumar->sumar($fraccio1,$fraccio2);
//Devuelve un objeto fraccion 

//"Resta de las fracciones: ";
$restar->restar($fraccio1,$fraccio2;

//Multiplicación de las fracciones: ";
$multiplicar->multiplicar($fraccio1,$fraccio2);

//"Dividisión de las fracciones: ";
$dividir->dividir($fraccio1,$fraccio2);

// "Fraccion irreductible: ";
$irreductible->obtenerFraccionIrreductible($fraccio1);

```


# MATRICES

Esta libreria sirve para crear y operar con matríces mátemáticas de cualquier dimensión.
La librería incluye una clase con métodos estáticos para hacer varias operaciones.

Ejemplo de crear una matríz:

```php
<?php
include ("matrix.php");

$newMatrix = new Matrix(2,3,[2,4,5,1,3,5]);
//A partir de ahora podremos usar este objeto para operar con el y mostrarlo por pantalla
```
Ejemplo de matríz no construible: 
```php
<?php
include ("matrix.php");
$newMatrix = new Matrix(2,3,[1,3]);
//Esto no devolverá nada y si hubiera salida por terminal mostraria un mensaje diciendolo.
Al constructor hay que pasarle el número de filas, el número de columna y un array con los valores de la matriz. El constructor devolverá un error si la matriz no es posible de construir
Para operar con las matrices hay que instanciar las clases de la operación que queramos hacer

```
Ejemplo de las operaciones:
```php
<?php
include ("matrix.php");

$newMatrix = new Matrix(3,2,[2,4,5,1,3,5]);
$oldMatrix = new Matrix(2,3,[2,4,5,1,3,5]);
//Si queremos imprimir la matríz
$oldMatrix->printMatrix();
//Salida esperada : 
//2 4
//5 1
//3 5
//Cualquier operación se tiene que llamar como método estático de la clase "MatrixOperation".
//Las operaciones siempre devolverán un nuevo objeto Matrix con el resultado.
$sumMatrix= MatrixOperation::sumar($newMatrix,$oldMatrix);//Guarda en la variable la matríz el objeto matriz resultado de la suma.

//Multiplicación de una matríz
$multMatrix = MatrixOperation::multiplicacion($newMatrix,$oldMatrix);
//Multiplicación de una matríz por un escalar
$escalarMatrix = MatrixOperation::multiplicarPorEscalar($newMatrix,3);
//Suma y resta de matrices
$sumaMatrix = MatrixOperation::sumar($newMatrix,$oldMatrix);
$restaMatrix = MatrixOperation::restar($newMatrix,$oldMatrix);
//Invertir matríz
$reversedMatrix = MatrixOperation::inversa($newMatrix);
```

# VECTORES


Esta librería sirve para hacer cálculos básicos entre dos vectores.

Ejemplo:

```php
<?php

require_once('library/escalar.php');
require_once('library/producte.php');
require_once('library/restar.php');
require_once('library/sumar.php');
require_once('library/unitari.php');
require_once('library/vector.php');


$vector1 = new Vector(1,2);
$vector2 = new Vector(1,4);
//Instanciamos los constructores de las classes para las operaciones.
//Con estos objetos podremos realizar las operaciones
$sumar = new Sumar();
$restar = new Restar();
$productePerEscalar = new Escalar();
$producte = new Producte();
$unitari = new Unitari();

// Suma de los vectores
$sumar->sumar($vector1,$vector2);

// Resta de las fracciones
$restar->restar($vector1,$vector2);
// 
// Producto per escalar
$productePerEscalar->productePerEscalar($vector1,3);
//
// Producte
$producte->producte($vector1,$vector2);
// 
// Unitari
$unitari->unitari($vector1);

```
