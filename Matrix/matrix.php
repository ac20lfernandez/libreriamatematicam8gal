<?php 

include ("operaciones.php");
/**
 * Clase que sirve como modelo del objeto Matriz.
 * Las matrices reciben por parámetro el número de filas
 * y columnas y un array con los valores. 
 * El constructor se encarga de validar que la matriz sea construible.
 * Ejemplo de matriz no construible:
 * new Matrix(2,3,[2,3,3])-> Devolverá un error ya que 
 * el array ha de tener 2*3 elementos.
 * 
 * La clase contiene getters y setters para las filas de las columnas
 * y setters para el conjunto de elementos.
 * 
 * 
 */
class Matrix {  

    private $mDim;
    private $nDim;
    private $elements;
    private $matrix=[[]];
    /**
     * Construye la matríz y settea los atributos que el modelo requiere
     * (El número de filas, columnas, el array de elementos y el array doble 
     * de la matríz ).
     * Si la matríz no es construible devuelve un string indicandolo
     * @param int $m
     * @param int $n
     * @param array[int] $elements
     * @return string
     */
    public function __construct($m,$n,$elements){
        if(gettype($m)=="integer" 
        && gettype($n)=="integer" && gettype($elements)=="array"
            && $m*$n===count($elements)){
                $this->mDim = $m;
                $this->elements = $elements;
                $this->nDim = $n;
                for($i=0;$i<$m;$i++){
                    $a=0;
                    $row=[];
                    while($a<$n){

                        array_push($row,array_shift($elements));
                        $a=$a+1;
                    }
                    
                    array_push($this->matrix,$row);
                }
            
        }else {
           echo "no se puede construir la matrix";
        }

        
    }
    /**
     * @return int
     */
    public function getM(){
        return $this->mDim;
    }
     /**
     * @return int
     */
    public function getN(){
        return $this->nDim;
    }
    /**
     * @param int $m
     * @return void
     */
    public function setM($m){
        $this->mDim=$m;
    }
      /**
     * @param int $n
     * @return void
     */
    public function setN($n){
        $this->nDim=$n;
    }
    /**
     * @return array[int];
     */
    public function getElements(){
        return $this->elements;
    }
    /**
     * @return int;
     */
    public function getDimension(){
        return $this->mDim * $this->nDim;
    }
/**
 * Devuelve un array que contiene arrays con las filas de la matríz
 * @return array[[int]]
 */
    public function getMatrix(){
        return $this->matrix;
    }   
    /**
     * Devuelve un string formateado de la matriz en una forma visualmente
     * entendible.
     * @return string
     */
    public function printMatrix(){
        foreach($this->matrix as $i){
            echo "<br>";
            foreach($i as $j){
                echo " ".$j;
            }
        }       
    }
}

?>