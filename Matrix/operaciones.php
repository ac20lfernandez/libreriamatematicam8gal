<?php
/**
 * Clase que contiene métodos estáticos
 * con las operaciones relativas a matríces.
 * 
 * La clase recibe principalmente objetos Matrix como argumentos
 * y un entero en uno de los métodos y siempre se devuelve
 * otro objeto Matrix. 
 * 
 */
class MatrixOperation{
/**
 * Método privado para validar 
 * que las matrices son compatibles con la operación suma.
 * @param  Matrix $matrix1
 * @param  Matrix $matrix2
 */
private static function validarMatricesMulti($matrix1,$matrix2){
        if($matrix1->getN() == $matrix2->getM() ){     
           return "ok";
        }else{
            return "no Multiplicable";
        }
}

/**
 * Método privado para validar 
 * que las matrices son compatibles con la operación de multiplicacón.
 * @param  Matrix $matrix1
 * @param  Matrix $matrix2
 */
private static function validarMatricesSuma($matrix1,$matrix2){
    if($matrix1->getM() == $matrix2->getM()
    &&
    $matrix1->getN() == $matrix2->getN()
    ){
       return "ok";
    }else{
        return "no sumable";
    }
}
/**
 * Devuelve el producto de la multiplicación
 * de las dos matríces pasadas por argumento.
 * La respuesta es un objeto Matrix
 * @param  Matrix $matrix1
 * @param  Matrix $matrix2
 * @return Matrix
 */
public static function multiplicar($matrix1,$matrix2){
        if(self ::validarMatricesMulti($matrix1,$matrix2)=="ok"){
            $result = [];
            $mDimRes= $matrix1->getM();
            $nDimRes = $matrix2->getN();
            $matrixA = $matrix1->getMatrix(); 
            $matrix2Tras=self::invertir($matrix2)->getMatrix();
            foreach($matrixA as $A){
                if(count($A)!=0){
                    $con = 0;
                    $i=1; 
                    while($con<$nDimRes){
                        $j=0;
                        $newAdd = 0;
                        foreach($A as $numA){
                            $newAdd += $numA*$matrix2Tras[$i][$j];
                            $j++; 
                        }
                        $con++;
                        $i++;
                        array_push($result,$newAdd);
                    }
                }
            }
        return new Matrix(
            $mDimRes,
            $nDimRes,
            $result);
        }else{
            echo "no multiplicable";
            return "no multiplicable";
        }
}

/**
 * Devuele la resta de las dos matrices indicadas
 * en forma de objeto Matrix 
 * @param  Matrix $matrix1
 * @param  Matrix $matrix2
 * @return Matrix
 */

public static function restar($matrix1,$matrix2){
   if(self ::validarMatricesSuma($matrix1,$matrix2)=="ok"){
       $result = [];
   for($i=1;$i<=$matrix1->getM();$i++){
        for($j=0;$j<$matrix2->getN();$j++){
        $newAdd = $matrix1->getMatrix()[$i][$j] - $matrix2->getMatrix()[$i][$j];
        array_push($result,$newAdd);
      }
    
   }

   return new Matrix(
    $matrix1->getN(),
    $matrix1->getM(),
    $result

   );
   }
}

/**
 * Devuele la suma de las dos matrices indicadas
 * en forma de objeto Matrix 
 * @param  Matrix $matrix1
 * @param  Matrix $matrix2
 * @return Matrix
 */
public static function sumar($matrix1,$matrix2){
   if(self ::validarMatricesSuma($matrix1,$matrix2)=="ok"){
       $result = [];
   for($i=1;$i<=$matrix1->getM();$i++){
        for($j=0;$j<$matrix2->getN();$j++){
        $newAdd = $matrix1->getMatrix()[$i][$j] + $matrix2->getMatrix()[$i][$j];
        array_push($result,$newAdd);
      }
    
   }

   return new Matrix(
    $matrix1->getN(),
    $matrix1->getM(),
    $result

   );
   }else{
       return "no sumable";
   }
}
/**
 * Devuele la matríz invertida de la matríz
 * (Se intercambian las filas por las columnas)
 * indicada en forma de objeto Matrix
 * @param  Matrix $matrix1
 * @param  Matrix $matrix2
 * @return Matrix
 */
public static function invertir($_matrix){

    $newM=$_matrix->getN();
    $newN=$_matrix->getM();
    $matrix = $_matrix->getMatrix();
    $aux = [];
    $i=0; 
    while($i<$newM){
        foreach($matrix as $A){
            if(count($A)!=0){
                array_push($aux,$A[$i]);
            }
        }
        if(count($A)!=0){
        $i++;
        }
    }
    return new Matrix($newM,$newN,$aux);
}
/**
 * Devuelve un objeto Matríx con todos sus valores
 * multiplicados por el escalar indicado
 * @param Matrix $_matrix
 * @param int $escalar
 * @return Matrix
 */
public static function multiplicarPorEscalar($_matrix,$escalar){

    $elementos = $_matrix->getElements();
    $aux = [];
    foreach($elementos as $valor){
        array_push($aux,($valor*$escalar));
    }

    return new Matrix(
        $_matrix->getM(),
        $_matrix->getN(),
        $aux
    );
}

}
