<?php

require_once('library/fraccio.php');
require_once('library/suma.php');
require_once('library/restar.php');
require_once('library/multiplicar.php');
require_once('library/dividir.php');
require_once('library/obtenerIrreductible.php');


$fraccio1 = new Fraccion(1,2);
$fraccio2 = new Fraccion(1,4);

$sumar = new Sumar();
$restar = new Restar;
$multiplicar = new Multiplicar();
$dividir = new Dividir();
$irreductible = new Irreductible();

echo "Suma de las fracciones: ";
print_r($sumar->sumar($fraccio1,$fraccio2));
echo "</br> ";
echo "</br> ";
echo "Resta de las fracciones: ";
print_r($restar->restar($fraccio1,$fraccio2));
echo "</br> ";
echo "</br> ";
echo "Multiplicar de las fracciones: ";
print_r($multiplicar->multiplicar($fraccio1,$fraccio2));
echo "</br> ";
echo "</br> ";
echo "Dividir de las fracciones: ";
print_r($dividir->dividir($fraccio1,$fraccio2));
echo "</br> ";
echo "</br> ";
echo "Fraccion irreductible: ";
print_r($irreductible->obtenerFraccionIrreductible($fraccio1));
echo "</br> ";

?>