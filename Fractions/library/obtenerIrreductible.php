<?php

/**
 * Clase que contiene la función para hacer el cálculo de una fracción irreductible
 */

class Irreductible {
     /**
      * La función devuelve la fracción irreductible de la fracción que se le pasa
      * @param Fraccion $a
      * @return Fraccion 
      */
    function obtenerFraccionIrreductible(Fraccion $a)
    {
        $numerador = $a->numerador;
        $denominador = $a->denominador;

        if ($numerador > $denominador) {
            $temp = $numerador;
            $numerador = $denominador;
            $denominador = $temp;
        }
        $mcd = array();
        for($i = 1; $i < ($numerador+1); $i++) {
            if ($numerador%$i == 0 and $denominador%$i == 0){
                $mcd[] = $i;
            }
                
        }
        $num = $numerador / $mcd[0];
        $den = $denominador / $mcd[0];
        return  new Fraccion($num, $den);
    } 
}
?>