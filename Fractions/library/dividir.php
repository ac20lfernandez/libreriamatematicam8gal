<?php
/**
 * Clsae que contiene la función para dividir dos funciones
 */
class Dividir{
    /**
     * Devuelve el resultado de dividir dos fracciones
     * @param Fraccion $a
     * @param Fraccion $b
     * @return Fraccion 
     */
    function dividir(Fraccion $a, Fraccion $b)
    {
        $num = $a->numerador * $b->denominador;
        $den = $a->denominador * $b->numerador;
        $fraccionDividida = new Fraccion($num, $den);
        return $fraccionDividida;
    }
}

?>