
<?php
/**
 * Clase que contiene la función para sumar vectores
 */

class Sumar {
    /**
      * Función que devuelve la resta de dos fracciones
      * @param Fraccion $a
      * @param Fraccion $b
      * @return Fraccion 
      */
    function sumar(Fraccion $a, Fraccion $b)
    {
        $num = ($a->denominador * $b->numerador)+($a->numerador * $b->denominador);
        $den = $a->denominador * $b->denominador;

        return new Fraccion($num, $den);
    }
}
?>