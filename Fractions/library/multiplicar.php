<?php
/**
 * Clase que contiene la función que permite multiplicar dos funciones
 */

class Multiplicar {
    /**
     * Clase que devuelve el producto de dos fracciones
     * @param Fraccion $a
     * @param Fraccion $b
     * @return Fraccion 
     */
    function multiplicar(Fraccion $a, Fraccion $b)
    {
        $num = $a->numerador * $b->numerador;
        $den = $a->denominador * $b->denominador;
        $fraccionMultiplicada = new Fraccion($num, $den);
        return $fraccionMultiplicada;
    }
}
?>